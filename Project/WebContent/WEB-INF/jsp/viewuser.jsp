<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>viewuser</title>
</head>
<body>
	<div class="alert alert-dark" role="alert">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</div>
	<div class="container">
		<h1 align="center">
			<b>ユーザー一覧</b>
		</h1>
	</div>

	<p align="right">
		<a href="http://localhost:8080/UserManagement/userEntry"> 新規登録 </a>
	</p>

	<div class="row justify-content-center align-items-center"
		style="height: 100vh">

		<div class="col-4">
			<form class="form-search" action="UserListServlet" method="post">
				<div class="row">
					<p class="col-4">ログインID</p>

					<div class="form-group col-8">
						<input type="text" class="form-control" name="loginId">
					</div>
				</div>
				<div class="row">
					<p class="col-4">ユーザー名</p>

					<div class="form-group col-8">
						<input type="text" class="form-control" name="name">
					</div>
					<div class="row">
						<p class="col-5">生年月日</p>

						<div class="form-group col-7">
							<input class="col-5" type="date" class="form-control"
								name="startBirthdate">～ <input class="col-5" type="date"
								class="form-control" name="endBirthdate">
						</div>
					</div>
				</div>

				<button type="submit" id="serch" class="btn btn-primary">検索</button>
			</form>
			<hr>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ログインID</th>
							<th>ユーザ名</th>
							<th>生年月日</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${userList}">
							<tr>
								<td>${user.loginId}</td>
								<td>${user.name}</td>
								<td>${user.birthDate}</td>
								<td>
								<c:if test="${userInfo.loginId == 'admin'}">
									<a class="btn btn-primary"
										href="UserDetail?id=${user.id}">詳細</a> <a
										class="btn btn-success"
										href="http://localhost:8080/UserManagement/UpdateServlet?id=${user.id}">更新</a>
										<a class="btn btn-danger"
										href="http://localhost:8080/UserManagement/DeleteServlet?id=${user.id}">削除</a>
								</c:if>

								<c:if test="${userInfo.loginId != 'admin'}">
									<a class="btn btn-primary" href="UserDetail?id=${user.id}">詳細</a>
								</c:if>
								<c:if test="${userInfo.loginId == user.loginId}">
									<a class="btn btn-success"
										href="http://localhost:8080/UserManagement/UpdateServlet?id=${user.id}">更新</a>
								</c:if>
							</td></tr>
						</c:forEach>
				</table>
			</div>
		</div>
	</div>
</body>
</html>