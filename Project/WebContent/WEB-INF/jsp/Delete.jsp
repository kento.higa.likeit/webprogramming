<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="ja">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Delete</title>
</head>
<body>
	<div class="alert alert-dark" role="alert">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</div>
	<div class="container">
		<h1 align="center">
			<b>ユーザー削除確認</b>
		</h1>
	</div>
	<div>
		<p align="center">ログインID：${UserDetail.loginId}</p>
		<p align="center">を本当に削除してよろしいでしょうか。
	</div>
		<form action="UserListServlet" method="get">
			<p align="center">
				<button type="submit" class="btn btn-light">キャンセル</button>
		</form>
		<form action="DeleteServlet" method="post">
		<input type="hidden"  name="id" value="${UserDetail.id}">
			<p align="center">
				<button type="submit" class="btn btn-light">OK</button>
			</p>
		</form>
</body>
</html>