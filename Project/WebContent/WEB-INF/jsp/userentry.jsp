<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>userentry</title>
</head>
<body>
	<div class="alert alert-dark" role="alert">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</div>
	<div class="container">
		<h1 align="center">
			<b>ユーザー新規登録</b>
		</h1>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">
				<h2 align="center">${errMsg}</h2>
			</div>
		</c:if>
		<div class="row justify-content-center align-items-center"
			style="height: 100vh">

			<div class="col-4">
			<form action="userEntry" method="post">
				<p align="left">ログインID</p>
					<div class="form-group">
						<input type="text" class="form-control" name="loginId" value="${loginId}">
					</div>
					<p align="left">パスワード</p>
					<div class="form-group">
						<input type="text" class="form-control" name="password">
					</div>
					<p align="left">パスワード(確認)</p>
					<div class="form-group">
						<input type="text" class="form-control" name="passwordConf">
					</div>
					<p align="left">ユーザー名</p>
					<div class="form-group">
						<input type="text" class="form-control" name="name" value="${Name}">
					</div>
					<p align="left">生年月日</p>
					<div class="form-group">
						<input type="date" class="form-control" name="birthDate" value="${birthDate}">
					</div>

					<button type="submit" class="btn btn-light">登録</button>

				<p align="left">
					<a href="http://localhost:8080/UserManagement/UserListServlet">戻る</a>
				</p>
				</form>
			</div>

		</div>

	</div>

</body>
</html>