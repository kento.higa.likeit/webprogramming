<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="ja">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>userinfo</title>
</head>
<body>
	<div class="alert alert-dark" role="alert">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</div>
	<div class="container">
		<h1 align="center">
			<b>ユーザー情報詳細参照</b>
		</h1>
	</div>
	<div>
		<p align="center">ログインID</p>
		<p align="center">${UserDetail.loginId}
		<p align="center">ユーザー名</p>
		<p align="center">${UserDetail.name}</p>
		<p align="center">生年月日</p>
		<p align="center">${UserDetail.birthDate}</p>
		<p align="center">登録日時</p>
		<p align="center">${UserDetail.createDate}</p>
		<p align="center">更新日時</p>
		<p align="center">${UserDetail.updateDate}</p>
	</div>
	<p align="center">
		<a href="http://localhost:8080/UserManagement/UserListServlet">戻る</a>
	</p>

</body>
</html>