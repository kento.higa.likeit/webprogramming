<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>update</title>
</head>
<body>
	<div class="alert alert-dark" role="alert">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</div>
	<div class="container">
		<h1 align="center">
			<b>ユーザー情報更新</b>
		</h1>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">
				<h2 align="center">${errMsg}</h2>
			</div>
		</c:if>
	</div>
	<div class="row justify-content-center align-items-center"
		style="height: 100vh">

		<div class="col-4">
			<p align="left">ログインID</p>

			<p align="center">${UserDetail.loginId}</p>

			<form action="UpdateServlet" method="post">
				<input type="hidden"  name="id" value="${UserDetail.id}">
				<p align="left">パスワード</p>
				<div class="form-group">
					<input type="text" class="form-control" name="password">
					<p align="left">パスワード(確認)</p>
					<div class="form-group">
						<input type="text" class="form-control" name="passwordConf">
						<p align="left">ユーザー名</p>
						<div class="form-group">
							<input type="text" class="form-control" name="name"
								value="${UserDetail.name}">
							<p align="left">生年月日</p>
							<div class="form-group">
								<input type="text" class="form-control" name="birthDate"
									value="${UserDetail.birthDate}">
							</div>
							<button type="submit" class="btn btn-light">更新</button>
						</div>
						<p align="left">
							<a href="http://localhost:8080/UserManagement/UserListServlet">戻る</a>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>