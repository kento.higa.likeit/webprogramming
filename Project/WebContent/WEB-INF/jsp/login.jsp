<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>login</title>
</head>
<body>
	<div class="container">
		<h1 align="center">
			<b>ログイン画面</b>
		</h1>
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		<h2 align="center">
		${errMsg}
		</h2>
		</div>
	</c:if>
		<div class="row justify-content-center align-items-center"
			style="height: 100vh">
			<div class="col-4">
				<p align="left">ログインID</p>
				<form class="form-signin" action="LoginServlet" method="post">
					<div class="form-group">
						<input type="text" class="form-control" name="loginId">
					</div>
					<p align="left">パスワード</p>
					<input type="password" class="form-control" name="password">
					<button type="submit" id="sendlogin" class="btn btn-primary">ログイン</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>