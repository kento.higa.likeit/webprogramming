package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			UserDao UserDao = new UserDao();
			String string = UserDao.encoding(password);
			pStmt.setString(2, string);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う。
			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void NewEntry(String loginId, String password, String Name, String birthDate) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date) VALUES (?,?,?,?,now(), now())";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, Name);
			pStmt.setString(3, birthDate);
			UserDao UserDao = new UserDao();
			String string = UserDao.encoding(password);
			pStmt.setString(4, string);

			int result = pStmt.executeUpdate();

			System.out.println(result);

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User LoginDetail(int id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthdayData = rs.getDate("birth_date");
			String createdayData = rs.getString("create_date");
			String updatedayData = rs.getString("update_date");

			return new User(Id, loginidData, nameData, birthdayData, null, createdayData, updatedayData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void Update(String password, String Name, String birthDate, int id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ? WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			UserDao UserDao = new UserDao();
			String string = UserDao.encoding(password);
			pStmt.setString(1, string);
			pStmt.setString(2, Name);
			pStmt.setString(3, birthDate);
			pStmt.setInt(4, id);

			int result = pStmt.executeUpdate();

			System.out.println(result);

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void Delete(int id) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			int result = pStmt.executeUpdate();

			System.out.println(result);

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String encoding(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		return new String(result);

	}

	public User AlreadyUsed(String loginId) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う。
			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");

			return new User(loginIdData, null);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public ArrayList<User> searchUser(String loginId, String name,
			String startBirthdate, String endBirthdate) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			if(!loginId.equals("")) {
				sql += " AND login_id = '" + loginId + "'";
			}

			if(!name.equals("")) {
				sql += " AND name like '%" + name + "%'";
			}

			System.out.println(sql);



			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
//
//			String sql = "SELECT * FROM user"
//					+ " WHERE login_id = ?"
//					+ " AND name LIKE ?"
//					+ " AND (birth_date >= ? AND birth_date <= ?)";
//
//			PreparedStatement pStmt = conn.prepareStatement(sql);
//			pStmt.setString(1, loginId);
//			pStmt.setString(2, "%" + name + "%");
//			pStmt.setString(3, startBirthdate);
//			pStmt.setString(4, endBirthdate);
//			ResultSet rs = pStmt.executeQuery();

			//検索結果が1件とは限らず、複数件取得したい為、コレクションを生成
			ArrayList<User> userList = new ArrayList<User>();

			//
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setName(rs.getString("name"));
				user.setBirthDate(rs.getDate("birth_date"));
				userList.add(user);
			}

			return userList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}