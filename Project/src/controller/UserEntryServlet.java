package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class userEntry
 */
@WebServlet("/userEntry")
public class UserEntryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEntryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる

		HttpSession session = request.getSession();

		if (session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");
			return;

		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userentry.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String Name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");




		UserDao userDao = new UserDao();
		User user = userDao.AlreadyUsed(loginId);


		/** 登録失敗時 **/
		if (user != null || !password.equals(passwordConf) || loginId.equals("")
				|| password.equals("")  || passwordConf.equals("") || Name.equals("") || birthDate.equals("")) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力した内容は正しくありません");

			//入力されていた内容をリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("Name", Name);
			request.setAttribute("birthDay", birthDate);

			// ユーザー新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userentry.jsp");
			dispatcher.forward(request, response);
			return;

		}

			/** 登録成功時 **/

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行

			UserDao userDAO = new UserDao();
			userDAO.NewEntry(loginId, password, Name, birthDate);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");

		}
}
