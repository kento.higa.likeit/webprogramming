package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる

		HttpSession session = request.getSession();

		if (session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");
			return;

		}

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/viewuser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		// TODO  未実装：検索処理全般

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String startBirthdate = request.getParameter("startBirthdate");
		String endBirthdate = request.getParameter("endBirthdate");

		// ユーザー情報を取得
		UserDao userDao = new UserDao();
		ArrayList<User> user = userDao.searchUser(loginId, name, startBirthdate, endBirthdate);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", user);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/viewuser.jsp");
		dispatcher.forward(request, response);

	}

}
