package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる

		HttpSession session = request.getSession();

		if (session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");
			return;

		}

		// URLからGETパラメータとしてIDを受け取る
		int id = Integer.parseInt(request.getParameter("id"));

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する

		UserDao userDao = new UserDao();
		User user = userDao.LoginDetail(id);

		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード

		request.setAttribute("UserDetail", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String Name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String birthDate = request.getParameter("birthDate");
		int id = Integer.parseInt(request.getParameter("id"));

		/** 登録失敗時 **/
		if (!password.equals(passwordConf)
				|| Name.equals("") || birthDate.equals("")) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力した内容は正しくありません");

			//入力されていた内容をリクエストスコープにセット
			request.setAttribute("Name", Name);
			request.setAttribute("birthDate", birthDate);

			// ユーザー新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userentry.jsp");
			dispatcher.forward(request, response);
			return;

		}

		/** 登録成功時 **/

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		userDao.Update(password, Name, birthDate, id);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}
}
